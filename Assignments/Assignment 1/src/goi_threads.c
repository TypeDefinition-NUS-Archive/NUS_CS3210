#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>
#include "util.h"
#include "exporter.h"
#include "settings.h"

// My Includes
#include <pthread.h>
#include <stdatomic.h>

// including the "dead faction": 0
#define MAX_FACTIONS 10

// this macro is here to make the code slightly more readable, not because it can be safely changed to
// any integer value; changing this to a non-zero value may break the code
#define DEAD_FACTION 0

// Structs
typedef struct {
    int *world, *inv, *wholeNewWorld;
} generationData;

typedef struct {
    // Multi-threading variables.
    atomic_int *totalDeathToll;
    pthread_barrier_t *barrier;

    // Per generation Data.
    generationData* genData;
    
    // Per thread data.
    int nRows, nCols;
    int nGenerations;
    int arrayStart, arrayEnd;
} ThreadArg;

/**
 * Specifies the number(s) of live neighbors of the same faction required for a dead cell to become alive.
 */
bool isBirthable(int n)
{
    return n == 3;
}

/**
 * Specifies the number(s) of live neighbors of the same faction required for a live cell to remain alive.
 */
bool isSurvivable(int n)
{
    return n == 2 || n == 3;
}

/**
 * Specifies the number of live neighbors of a different faction required for a live cell to die due to fighting.
 */
bool willFight(int n)
{
    return n > 0;
}

/**
 * Computes and returns the next state of the cell specified by row and col based on currWorld and invaders. Sets *diedDueToFighting to
 * true if this cell should count towards the death toll due to fighting.
 * 
 * invaders can be NULL if there are no invaders.
 */
int getNextState(const int *currWorld, const int *invaders, int nRows, int nCols, int row, int col, bool *diedDueToFighting)
{
    // we'll explicitly set if it was death due to fighting
    *diedDueToFighting = false;

    // faction of this cell
    int cellFaction = getValueAt(currWorld, nRows, nCols, row, col);

    // did someone just get landed on?
    if (invaders != NULL && getValueAt(invaders, nRows, nCols, row, col) != DEAD_FACTION)
    {
        *diedDueToFighting = cellFaction != DEAD_FACTION;
        return getValueAt(invaders, nRows, nCols, row, col);
    }

    // tracks count of each faction adjacent to this cell
    int neighborCounts[MAX_FACTIONS];
    memset(neighborCounts, 0, MAX_FACTIONS * sizeof(int));

    // count neighbors (and self)
    for (int dy = -1; dy <= 1; dy++)
    {
        for (int dx = -1; dx <= 1; dx++)
        {
            int faction = getValueAt(currWorld, nRows, nCols, row + dy, col + dx);
            if (faction >= DEAD_FACTION)
            {
                neighborCounts[faction]++;
            }
        }
    }

    // we counted this cell as its "neighbor"; adjust for this
    neighborCounts[cellFaction]--;

    if (cellFaction == DEAD_FACTION)
    {
        // this is a dead cell; we need to see if a birth is possible:
        // need exactly 3 of a single faction; we don't care about other factions

        // by default, no birth
        int newFaction = DEAD_FACTION;

        // start at 1 because we ignore dead neighbors
        for (int faction = DEAD_FACTION + 1; faction < MAX_FACTIONS; faction++)
        {
            int count = neighborCounts[faction];
            if (isBirthable(count))
            {
                newFaction = faction;
            }
        }

        return newFaction;
    }
    else
    {
        /** 
         * this is a live cell; we follow the usual rules:
         * Death (fighting): > 0 hostile neighbor
         * Death (underpopulation): < 2 friendly neighbors and 0 hostile neighbors
         * Death (overpopulation): > 3 friendly neighbors and 0 hostile neighbors
         * Survival: 2 or 3 friendly neighbors and 0 hostile neighbors
         */

        int hostileCount = 0;
        for (int faction = DEAD_FACTION + 1; faction < MAX_FACTIONS; faction++)
        {
            if (faction == cellFaction)
            {
                continue;
            }
            hostileCount += neighborCounts[faction];
        }

        if (willFight(hostileCount))
        {
            *diedDueToFighting = true;
            return DEAD_FACTION;
        }

        int friendlyCount = neighborCounts[cellFaction];
        if (!isSurvivable(friendlyCount))
        {
            return DEAD_FACTION;
        }

        return cellFaction;
    }
}

int generate_new_cells(int *world, int *inv, int *wholeNewWorld, int arrayStart, int arrayEnd, int nRows, int nCols)
{
    int deathToll = 0;
    
    for (int i = arrayStart; i < arrayEnd; i++)
    {
        int row = i / nCols;
        int col = i % nCols;
        bool diedDueToFighting;
        int nextState = getNextState(world, inv, nRows, nCols, row, col, &diedDueToFighting);
        setValueAt(wholeNewWorld, nRows, nCols, row, col, nextState);
        if (diedDueToFighting) { deathToll++; }
    }

    return deathToll;
}

void* worker_thread(void* arg)
{
    // Get thread argument.
    ThreadArg* threadArg = (ThreadArg*)arg;

    // get new states for each cell
    while (threadArg->nGenerations--)
    {
        // Wait for world, inv and wholeNewWorld to be updated.
        pthread_barrier_wait(threadArg->barrier);

        // Run simulation.
        int deathToll = generate_new_cells(
            threadArg->genData->world,
            threadArg->genData->inv,
            threadArg->genData->wholeNewWorld,
            threadArg->arrayStart,
            threadArg->arrayEnd,
            threadArg->nRows,
            threadArg->nCols);

        // Update total death toll.
        (*threadArg->totalDeathToll) += deathToll;

        // Tell the main thread that world, inv and wholeNewWorld can be deleted.
        pthread_barrier_wait(threadArg->barrier);
    }
}

/**
 * The main simulation logic.
 * 
 * goi does not own startWorld, invasionTimes or invasionPlans and should not modify or attempt to free them.
 * nThreads is the number of threads to simulate with. It is ignored by the sequential implementation.
 */
int goi(int nThreads, int nGenerations, const int *startWorld, int nRows, int nCols, int nInvasions, const int *invasionTimes, int **invasionPlans)
{
    // init the world!
    // we make a copy because we do not own startWorld (and will perform free() on world)
    int *world = malloc(sizeof(int) * nRows * nCols);
    if (world == NULL)
    {
        return -1;
    }
    memcpy(world, startWorld, sizeof(int) * nRows * nCols);

#if PRINT_GENERATIONS
    printf("\n=== WORLD 0 ===\n");
    printWorld(world, nRows, nCols);
#endif

#if EXPORT_GENERATIONS
    exportWorld(world, nRows, nCols);
#endif

    // death toll due to fighting
    atomic_int totalDeathToll = 0;

    // Barriers for synchronisation.
    pthread_barrier_t barrier;
    pthread_barrier_init(&barrier, NULL, nThreads);
    pthread_barrier_t endBarrier;
    pthread_barrier_init(&endBarrier, NULL, nThreads);

    // Simulation Data
    generationData genData;
    memset(&genData, 0, sizeof(generationData));

    // Thread arguments
    ThreadArg threadArgs[nThreads];
    memset(threadArgs, 0, sizeof(threadArgs));

    // Divide up the job.
    // nThreads = numWorkerThreads + 1 main thread.
    int numWorkerThreads = nThreads - 1;
    int minCellsPerThread = (nRows * nCols) / nThreads; // Each thread should have minimumly this amount of workload.
    int extraCells = (nRows * nCols) % nThreads; // Some threads will have to do 1 more job than others.
    int arrayStart = 0;
    for (int i = 0; i < nThreads; ++i) {
        threadArgs[i].totalDeathToll = &totalDeathToll;
        threadArgs[i].barrier = &barrier;
        threadArgs[i].genData = &genData;
        threadArgs[i].nRows = nRows;
        threadArgs[i].nCols = nCols;
        threadArgs[i].nGenerations = nGenerations;
        threadArgs[i].arrayStart = arrayStart;
        threadArgs[i].arrayEnd = arrayStart + minCellsPerThread;

        if (i < extraCells) {
            threadArgs[i].arrayEnd++;
        }
        arrayStart = threadArgs[i].arrayEnd;
    }

    // Spawn nThreads-1 worker threads.
    pthread_t workerThreadIDs[numWorkerThreads];
    for (int i = 0; i < numWorkerThreads; ++i) {
        pthread_create(&workerThreadIDs[i], NULL, worker_thread, (void*)&threadArgs[i]);
    }

    // Begin simulating
    int invasionIndex = 0;
    for (int i = 1; i <= nGenerations; i++)
    {
        // is there an invasion this generation?
        int *inv = NULL;
        if (invasionIndex < nInvasions && i == invasionTimes[invasionIndex])
        {
            // we make a copy because we do not own invasionPlans
            inv = malloc(sizeof(int) * nRows * nCols);
            if (inv == NULL)
            {
                free(world);
                return -1;
            }
            memcpy(inv, invasionPlans[invasionIndex++], nRows * nCols * sizeof(int));
        }

        // create the next world state
        int *wholeNewWorld = malloc(sizeof(int) * nRows * nCols);
        if (wholeNewWorld == NULL)
        {
            if (inv != NULL)
            {
                free(inv);
            }
            free(world);
            return -1;
        }

        // Update generation data.
        genData.world = world;
        genData.inv = inv;
        genData.wholeNewWorld = wholeNewWorld;

        // Wait for world, inv and wholeNewWorld to be updated.
        pthread_barrier_wait(&barrier);

        // Run simulation.
        int deathToll = generate_new_cells(
            world,
            inv,
            wholeNewWorld,
            threadArgs[numWorkerThreads].arrayStart,
            threadArgs[numWorkerThreads].arrayEnd,
            threadArgs[numWorkerThreads].nRows,
            threadArgs[numWorkerThreads].nCols);

        // Update total death toll.
        totalDeathToll += deathToll;

        // Wait for workers to be done so that world, inv and wholeNewWorld can be cleaned up.
        pthread_barrier_wait(&barrier);

        // delete inv
        if (inv != NULL)
        {
            free(inv);
        }

        // swap worlds
        free(world);
        world = wholeNewWorld;

#if PRINT_GENERATIONS
        printf("\n=== WORLD %d ===\n", i);
        printWorld(world, nRows, nCols);
#endif

#if EXPORT_GENERATIONS
        exportWorld(world, nRows, nCols);
#endif
    }

    // Join worker threads.
    for (int i = 0; i < numWorkerThreads; ++i)
    {
        pthread_join(workerThreadIDs[i], NULL);
    }

    free(world);
    return totalDeathToll;
}
