log_file=goi_1t
out_file=goi

echo -e "goi timing (1 thread)\n" > ./timing_logs/${log_file}.log

cat /proc/cpuinfo | grep 'vendor' | uniq >> ./timing_logs/${log_file}.log
cat /proc/cpuinfo | grep 'model name' | uniq >> ./timing_logs/${log_file}.log
cat /proc/cpuinfo | grep processor | wc -l >> ./timing_logs/${log_file}.log

echo -e "\nsample0.in" >> ./timing_logs/${log_file}.log
{ time ./goi ./sample_inputs/sample0.in ./outputs/${out_file}0.out 1; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample1.in" >> ./timing_logs/${log_file}.log
{ time ./goi ./sample_inputs/sample1.in ./outputs/${out_file}1.out 1; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample2.in" >> ./timing_logs/${log_file}.log
{ time ./goi ./sample_inputs/sample2.in ./outputs/${out_file}2.out 1; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample3.in" >> ./timing_logs/${log_file}.log
{ time ./goi ./sample_inputs/sample3.in ./outputs/${out_file}3.out 1; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample4.in" >> ./timing_logs/${log_file}.log
{ time ./goi ./sample_inputs/sample4.in ./outputs/${out_file}4.out 1; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample5.in" >> ./timing_logs/${log_file}.log
{ time ./goi ./sample_inputs/sample5.in ./outputs/${out_file}5.out 1; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample6.in" >> ./timing_logs/${log_file}.log
{ time ./goi ./sample_inputs/sample6.in ./outputs/${out_file}6.out 1; } 2>> ./timing_logs/${log_file}.log