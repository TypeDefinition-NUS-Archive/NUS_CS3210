num_threads=$1
log_file=goi_threads_${num_threads}t
out_file=goi_threads

echo -e "valgrind goi_threads\n"

cat /proc/cpuinfo | grep 'vendor' | uniq
cat /proc/cpuinfo | grep 'model name' | uniq
cat /proc/cpuinfo | grep processor | wc -l

echo -e "\nsample6.in"
valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=./valgrind_logs/${log_file}.log \
    ./goi_threads ./sample_inputs/sample6.in ./outputs/${out_file}6.out ${num_threads}