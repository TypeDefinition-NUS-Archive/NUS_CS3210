#ifndef GOI_H
#define GOI_H

__host__ int goi(dim3 gd, dim3 bd, int nGenerations, const int *startWorld, int nRows, int nCols, int nInvasions, const int *invasionTimes, int **invasionPlans);

#endif
