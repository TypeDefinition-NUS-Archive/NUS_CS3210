/**
 * NOTE TO STUDENTS: you should not need to modify this file.
 * You can if you wish to, but incorrectly modifying it may lead to an incorrect program which you will be penalized for.
 * 
 * Also, if you modify this file (e.g. for some optimization) and wish it to be taken into consideration for grading,
 * please make a note of it in your report. Otherwise, we will NOT consider it when grading.
 */

#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include "goi.h"

int readParam(FILE *fp, char **line, size_t *len, int *param);
int readWorldLayout(FILE *fp, char **line, size_t *len, int *world, int nRows, int nCols);

/**
 * Handles input, output and file open/close operations. Delegates simulation to goi.
 */
int main(int argc, char *argv[])
{
    FILE *inputFile;
    FILE *outputFile;
    char *line = NULL;
    size_t len = 0;

    int gd[3]; // Grid Dimensions
    int bd[3]; // Block Dimensions

    int nGenerations;
    int nRows;
    int nCols;
    int *startWorld;
    int nInvasions;
    int *invasionTimes;
    int **invasionPlans;

    if (argc < 9)
    {
        std::exit(EXIT_FAILURE);
    }

    // Parse input file.
    inputFile = fopen(argv[1], "r");

    // Parse output file.
    outputFile = fopen(argv[2], "w");

    // Parse grid dimensions.
    sscanf(argv[3], "%d", &gd[0]);
    sscanf(argv[4], "%d", &gd[1]);
    sscanf(argv[5], "%d", &gd[2]);

    // Parse block dimensions.
    sscanf(argv[6], "%d", &bd[0]);
    sscanf(argv[7], "%d", &bd[1]);
    sscanf(argv[8], "%d", &bd[2]);

    // Read nGenerations
    readParam(inputFile, &line, &len, &nGenerations);
    // Read nRows
    readParam(inputFile, &line, &len, &nRows);
    // Read nCols
    readParam(inputFile, &line, &len, &nCols);

    // Read start world
    startWorld = (int*)malloc(sizeof(int) * nRows * nCols);
    readWorldLayout(inputFile, &line, &len, startWorld, nRows, nCols);

    // Read n_nnvasions
    readParam(inputFile, &line, &len, &nInvasions);

    // Read invasions
    invasionTimes = (int*)malloc(sizeof(int) * nInvasions);
    invasionPlans = (int**)malloc(sizeof(int *) * nInvasions);
    for (int i = 0; i < nInvasions; i++)
    {
        readParam(inputFile, &line, &len, invasionTimes + i);
        invasionPlans[i] = (int*)malloc(sizeof(int) * nRows * nCols);
        readWorldLayout(inputFile, &line, &len, invasionPlans[i], nRows, nCols);
    }

    // we're done with the file
    fclose(inputFile);
    if (line)
    {
        free(line);
    }

    // run the simulation
    int warDeathToll = goi(dim3(gd[0], gd[1], gd[2]), dim3(bd[0], bd[1], bd[2]), nGenerations, startWorld, nRows, nCols, nInvasions, invasionTimes, invasionPlans);

    // output the result
    fprintf(outputFile, "%d", warDeathToll);
    fclose(outputFile);

    // free everything!
    for (int i = 0; i < nInvasions; i++)
    {
        free(invasionPlans[i]);
    }
    free(invasionTimes);
    free(invasionPlans);
    free(startWorld);
}

// readParam reads one integer from a line into param, advancing the read head to the next line.
// -1 is returned on error.
int readParam(FILE *fp, char **line, size_t *len, int *param)
{
    if (getline(line, len, fp) == -1 ||
        sscanf(*line, "%d", param) != 1)
    {
        return -1;
    }
    return 0;
}

// readWorldLayout reads a world layout specified by nRows and nCols, advancing the read head by
// nRows number of lines. -1 is returned on error.
int readWorldLayout(FILE *fp, char **line, size_t *len, int *world, int nRows, int nCols)
{
    for (int row = 0; row < nRows; row++)
    {
        if (getline(line, len, fp) == -1)
        {
            return -1;
        }

        char *p = *line;
        for (int col = 0; col < nCols; col++)
        {
            char *end;
            int cell = strtol(p, &end, 10);

            // unexpected end
            if (cell == 0 && end == p)
            {
                return -1;
            }

            // other errors
            if (errno == EINVAL || errno == ERANGE)
            {
                return -1;
            }

            world[row * nCols + col] = cell;
            p = end;
        }
    }

    return 0;
}