# Grid Dimensions
gd_x=$1
gd_y=$2
gd_z=$3

# Block Dimensions
bd_x=$4
bd_y=$5
bd_z=$6

# Input/Output Folders
input_folder=sample_inputs
output_folder=outputs
logs_folder=logs

# Program Name, Output Filename & Log Filename
program_name=goi_cuda
out_file=goi_cuda_gd\(${gd_x}_${gd_y}_${gd_z}\)_bd\(${bd_x}_${bd_y}_${bd_z}\)
log_file=goi_cuda_gd\(${gd_x}_${gd_y}_${gd_z}\)_bd\(${bd_x}_${bd_y}_${bd_z}\)

# Print Title
echo -e "Simulating Game of Invasions on CUDA.\nCUDA Grid Dimension: (${gd_x}, ${gd_y}, ${gd_z}) | CUDA Block Dimensions: (${bd_x}, ${bd_y}, ${bd_z})\n" > ./${logs_folder}/${log_file}.log

# Print Hardware Info
cat /proc/cpuinfo | grep 'vendor' | uniq >> ./${logs_folder}/${log_file}.log
cat /proc/cpuinfo | grep 'model name' | uniq >> ./${logs_folder}/${log_file}.log
cat /proc/cpuinfo | grep processor | wc -l >> ./${logs_folder}/${log_file}.log

# Run Simulations
echo -e "\nsample0.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample0.in ./${output_folder}/${out_file}_0.out ${gd_x} ${gd_y} ${gd_z} ${bd_x} ${bd_y} ${bd_z}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample1.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample1.in ./${output_folder}/${out_file}_1.out ${gd_x} ${gd_y} ${gd_z} ${bd_x} ${bd_y} ${bd_z}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample2.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample2.in ./${output_folder}/${out_file}_2.out ${gd_x} ${gd_y} ${gd_z} ${bd_x} ${bd_y} ${bd_z}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample3.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample3.in ./${output_folder}/${out_file}_3.out ${gd_x} ${gd_y} ${gd_z} ${bd_x} ${bd_y} ${bd_z}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample4.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample4.in ./${output_folder}/${out_file}_4.out ${gd_x} ${gd_y} ${gd_z} ${bd_x} ${bd_y} ${bd_z}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample5.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample5.in ./${output_folder}/${out_file}_5.out ${gd_x} ${gd_y} ${gd_z} ${bd_x} ${bd_y} ${bd_z}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample6.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample6.in ./${output_folder}/${out_file}_6.out ${gd_x} ${gd_y} ${gd_z} ${bd_x} ${bd_y} ${bd_z}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample7.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample7.in ./${output_folder}/${out_file}_7.out ${gd_x} ${gd_y} ${gd_z} ${bd_x} ${bd_y} ${bd_z}; } 2>> ./${logs_folder}/${log_file}.log