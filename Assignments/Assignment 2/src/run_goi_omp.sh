# Number of Threads
num_threads=$1

# Input/Output Folders
input_folder=sample_inputs
output_folder=outputs
logs_folder=logs

# Program Name, Output Filename & Log Filename
program_name=goi_omp
out_file=goi_omp_${num_threads}t
log_file=goi_omp_${num_threads}t

# Print Title
echo -e "Simulating Game of Invasions on OpenMP.\nNumber of Threads: ${num_threads}\n" > ./${logs_folder}/${log_file}.log

# Print Hardware Info
cat /proc/cpuinfo | grep 'vendor' | uniq >> ./${logs_folder}/${log_file}.log
cat /proc/cpuinfo | grep 'model name' | uniq >> ./${logs_folder}/${log_file}.log
cat /proc/cpuinfo | grep processor | wc -l >> ./${logs_folder}/${log_file}.log

# Run Simulations
echo -e "\nsample0.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample0.in ./${output_folder}/${out_file}_0.out ${num_threads}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample1.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample1.in ./${output_folder}/${out_file}_1.out ${num_threads}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample2.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample2.in ./${output_folder}/${out_file}_2.out ${num_threads}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample3.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample3.in ./${output_folder}/${out_file}_3.out ${num_threads}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample4.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample4.in ./${output_folder}/${out_file}_4.out ${num_threads}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample5.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample5.in ./${output_folder}/${out_file}_5.out ${num_threads}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample6.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample6.in ./${output_folder}/${out_file}_6.out ${num_threads}; } 2>> ./${logs_folder}/${log_file}.log

echo -e "\nsample7.in" >> ./${logs_folder}/${log_file}.log
{ time ./${program_name} ./${input_folder}/sample7.in ./${output_folder}/${out_file}_7.out ${num_threads}; } 2>> ./${logs_folder}/${log_file}.log