#!/bin/bash

# Source & Destination Folders
nusnetid=E0544466
studentid=A0218430N
src=src

# Create Temporary Folders
mkdir ./temp

# Copy Report
cp ./${studentid}_report.pdf ./temp/${studentid}_report.pdf

# Copy GOI
cp ./${src}/cuda/goi.h ./temp/goi.h
cp ./${src}/cuda/goi_cuda.cu ./temp/goi_cuda.cu
cp ./${src}/cuda/main.cu ./temp/main.cu

# Copy Makefile & Test Cases
cp ./${src}/cuda/Makefile ./temp/Makefile
cp -r ./${src}/sample_inputs/ ./temp/testcases/

# ZIP Folders
cd ./temp
zip -r ./${studentid}.zip ./*
cd ../
mv ./temp/${studentid}.zip ./${studentid}.zip

# Delete Temporary Folders
rm -r temp

# Run Checker
./${src}/check_zip.sh ${studentid}.zip