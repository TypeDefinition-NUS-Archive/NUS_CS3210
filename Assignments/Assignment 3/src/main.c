#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include "tasks.h"
#include "utils.h"

#define MAX_CHARS 1024 * 1024
#define MAX_ELEMENTS 1024

enum tags
{
    tags_file_contents = 0,
    tags_worker_ready,
    tags_partitions,
    tags_results,
};

typedef struct
{
    char *input_files_dir, *output_file_name;
    int num_files, num_map_workers, num_reduce_workers, map_reduce_task_num;
    int world_size, rank;
    MapTaskOutput *(*map)(char *);
} arg_t;

typedef struct
{
    char key[8];
    int vals[MAX_ELEMENTS];
    int num_vals;
} key_counter_t;

void master(const arg_t *args)
{
    // Give each map worker it's task.
    for (int i = 0; i < args->num_files; ++i)
    {
        // Open & get file size.
        char file_name[64] = {0};
        sprintf(file_name, "%s/%d.txt", args->input_files_dir, i);
        FILE *file = fopen(file_name, "r");
        fseek(file, 0, SEEK_END);
        long file_size = ftell(file);
        rewind(file);

        // Copy file contents into a buffer & close file.
        char file_contents[MAX_CHARS] = {0};
        fread(file_contents, sizeof(char), file_size, file);
        fclose(file);

        // Get available worker.
        MPI_Status status;
        MPI_Recv(NULL, 0, MPI_BYTE, MPI_ANY_SOURCE, tags_worker_ready, MPI_COMM_WORLD, &status);

        // Send file contents.
        MPI_Send(file_contents, file_size, MPI_CHAR, status.MPI_SOURCE, tags_file_contents, MPI_COMM_WORLD);
    }

    // Inform each map worker to terminate.
    for (int i = 0; i < args->num_map_workers; ++i)
    {
        MPI_Status status;
        MPI_Recv(NULL, 0, MPI_BYTE, 1 + i, tags_worker_ready, MPI_COMM_WORLD, &status);
        MPI_Send(NULL, 0, MPI_CHAR, 1 + i, tags_file_contents, MPI_COMM_WORLD);
    }

    // Receive results from reduce workers.
    int output_fd = open(args->output_file_name, O_RDWR | O_CREAT | O_TRUNC, S_IRWXG | S_IRWXU | S_IRWXO);
    for (int i = 0; i < args->num_reduce_workers; ++i)
    {
        MPI_Status status;
        char results[MAX_CHARS] = {0};
        MPI_Recv(results, sizeof(results), MPI_BYTE, MPI_ANY_SOURCE, tags_results, MPI_COMM_WORLD, &status);
        write(output_fd, results, status._ucount);
    }
    close(output_fd);
}

void map_worker(const arg_t *args)
{
    int num_outputs = 0;
    MapTaskOutput *outputs[MAX_ELEMENTS] = {0};
    while (true)
    {
        // Inform master that we are ready to receive a task.
        MPI_Send(NULL, 0, MPI_BYTE, 0, tags_worker_ready, MPI_COMM_WORLD);

        // Receive file contents.
        MPI_Status status;
        char file_contents[MAX_CHARS] = {0};
        MPI_Recv(file_contents, sizeof(file_contents), MPI_CHAR, 0, tags_file_contents, MPI_COMM_WORLD, &status);

        // Terminate signal.
        if (status._ucount == 0)
        {
            break;
        }

        // Map
        MapTaskOutput* output = args->map(file_contents);
        outputs[num_outputs++] = output;
    }

    // Partition the outputs. Number of partitions = Number of reduce workers.
    int num_partitions = args->num_reduce_workers;
    int *num_elements = calloc(num_partitions, sizeof(int));
    KeyValue **parts = calloc(num_partitions, sizeof(KeyValue *));
    for (int i = 0; i < num_partitions; ++i)
    {
        parts[i] = calloc(MAX_ELEMENTS, sizeof(KeyValue));
    }

    for (int i = 0; i < num_outputs; ++i)
    {
        MapTaskOutput* output = outputs[i];
        for (int j = 0; j < output->len; ++j)
        {
            int p = partition(output->kvs[j].key, num_partitions);
            KeyValue *src = &output->kvs[j];
            KeyValue *dst = &parts[p][num_elements[p]++];
            memcpy(dst, src, sizeof(KeyValue));

            // printf("Dst Key %s\n", parts[p][num_elements[p] - 1].key);
        }
    }

    // Send the partitions to the reducers.
    for (int i = 0; i < num_partitions; ++i)
    {
        MPI_Send(parts[i], num_elements[i] * sizeof(KeyValue), MPI_BYTE, 1 + args->num_map_workers + i, tags_partitions, MPI_COMM_WORLD);
    }

    // Free memory.
    for (int i = 0; i < num_outputs; ++i)
    {
        free_map_task_output(outputs[i]);
    }

    free(num_elements);
    for (int i = 0; i < num_partitions; ++i)
    {
        free(parts[i]);
    }
    free(parts);
}

void reduce_worker(const arg_t *args)
{
    int num_keys = 0;
    key_counter_t counters[MAX_ELEMENTS] = {0};

    // Receive data from all map workers.
    for (int i = 0; i < args->num_map_workers; ++i)
    {
        KeyValue kvs[MAX_ELEMENTS] = {0};
        MPI_Status status;
        MPI_Recv(kvs, sizeof(kvs), MPI_BYTE, MPI_ANY_SOURCE, tags_partitions, MPI_COMM_WORLD, &status);

        // Hash key-value pairs.
        int num_kvs = status._ucount / sizeof(KeyValue);
        for (int k = 0; k < num_kvs; ++k)
        {
            bool new_key = true;

            for (int j = 0; j < num_keys; ++j)
            {
                if (!strcmp(kvs[k].key, counters[j].key))
                {
                    counters[j].vals[counters[j].num_vals++] += kvs[k].val;
                    new_key = false;
                    break;
                }
            }

            if (new_key)
            {
                memcpy(counters[num_keys].key, kvs[k].key, 8 * sizeof(char));
                counters[num_keys].vals[counters[num_keys].num_vals] += kvs[k].val;
                ++counters[num_keys].num_vals;
                ++num_keys;
            }
        }
    }

    // Generate result.
    char result[MAX_CHARS] = {0};
    for (int i = 0; i < num_keys; ++i)
    {
        char line[64] = {0};
        KeyValue kv = reduce(counters[i].key, counters[i].vals, counters[i].num_vals);
        sprintf(line, "%s %d\n", kv.key, kv.val);
        strcat(result, line);
    }

    // Send data back to master.
    MPI_Send(result, strlen(result), MPI_CHAR, 0, tags_results, MPI_COMM_WORLD);
}

int main(int argc, char **argv)
{
    // Initialise MPI.
    MPI_Init(&argc, &argv);

    // Get MPI world size and rank.
    int world_size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Get command-line parameters.
    char *input_files_dir = argv[1];
    int num_files = atoi(argv[2]);
    int num_map_workers = atoi(argv[3]);
    int num_reduce_workers = atoi(argv[4]);
    char *output_file_name = argv[5];
    int map_reduce_task_num = atoi(argv[6]);

    // Identify the specific map maption to use.
    MapTaskOutput *(*map)(char *);
    switch (map_reduce_task_num)
    {
    case 1:
        map = &map1;
        break;
    case 2:
        map = &map2;
        break;
    case 3:
        map = &map3;
        break;
    }

    // maption arguments.
    arg_t args;
    args.input_files_dir = input_files_dir;
    args.num_files = num_files;
    args.num_map_workers = num_map_workers;
    args.num_reduce_workers = num_reduce_workers;
    args.output_file_name = output_file_name;
    args.map_reduce_task_num = atoi(argv[6]);
    args.world_size = world_size;
    args.rank = rank;
    args.map = map;

    // Distinguish between master, map workers and reduce workers
    if (rank == 0)
    {
        master(&args);
    }
    else if ((rank >= 1) && (rank <= num_map_workers))
    {
        map_worker(&args);
    }
    else
    {
        reduce_worker(&args);
    }

    //Clean up
    MPI_Finalize();
    return 0;
}
