#!/bin/bash

num_input_files=6
input_files_dir=./sample_input_files
output_file=./output/time_test3.output
log_file=./logs/time_test3.log
map_reduce_task_id=3

echo -e "OpenMPI Timings Test" > ${log_file}

num_map_workers=1
num_reduce_workers=1
num_mpi_processes=$((num_reduce_workers + num_map_workers + 1))

echo -e "\nMap Workers: ${num_map_workers}, Reduce Workers: ${num_reduce_workers}" >> ${log_file}
{ time mpirun --oversubscribe -np ${num_mpi_processes} ./a03 ${input_files_dir} ${num_input_files} ${num_map_workers} ${num_reduce_workers} ${output_file} ${map_reduce_task_id}; } 2>> ${log_file}

num_map_workers=2
num_reduce_workers=2
num_mpi_processes=$((num_reduce_workers + num_map_workers + 1))

echo -e "\nMap Workers: ${num_map_workers}, Reduce Workers: ${num_reduce_workers}" >> ${log_file}
{ time mpirun --oversubscribe -np ${num_mpi_processes} ./a03 ${input_files_dir} ${num_input_files} ${num_map_workers} ${num_reduce_workers} ${output_file} ${map_reduce_task_id}; } 2>> ${log_file}

num_map_workers=3
num_reduce_workers=3
num_mpi_processes=$((num_reduce_workers + num_map_workers + 1))

echo -e "\nMap Workers: ${num_map_workers}, Reduce Workers: ${num_reduce_workers}" >> ${log_file}
{ time mpirun --oversubscribe -np ${num_mpi_processes} ./a03 ${input_files_dir} ${num_input_files} ${num_map_workers} ${num_reduce_workers} ${output_file} ${map_reduce_task_id}; } 2>> ${log_file}

num_map_workers=4
num_reduce_workers=4
num_mpi_processes=$((num_reduce_workers + num_map_workers + 1))

echo -e "\nMap Workers: ${num_map_workers}, Reduce Workers: ${num_reduce_workers}" >> ${log_file}
{ time mpirun --oversubscribe -np ${num_mpi_processes} ./a03 ${input_files_dir} ${num_input_files} ${num_map_workers} ${num_reduce_workers} ${output_file} ${map_reduce_task_id}; } 2>> ${log_file}

num_map_workers=5
num_reduce_workers=5
num_mpi_processes=$((num_reduce_workers + num_map_workers + 1))

echo -e "\nMap Workers: ${num_map_workers}, Reduce Workers: ${num_reduce_workers}" >> ${log_file}
{ time mpirun --oversubscribe -np ${num_mpi_processes} ./a03 ${input_files_dir} ${num_input_files} ${num_map_workers} ${num_reduce_workers} ${output_file} ${map_reduce_task_id}; } 2>> ${log_file}

num_map_workers=6
num_reduce_workers=6
num_mpi_processes=$((num_reduce_workers + num_map_workers + 1))

echo -e "\nMap Workers: ${num_map_workers}, Reduce Workers: ${num_reduce_workers}" >> ${log_file}
{ time mpirun --oversubscribe -np ${num_mpi_processes} ./a03 ${input_files_dir} ${num_input_files} ${num_map_workers} ${num_reduce_workers} ${output_file} ${map_reduce_task_id}; } 2>> ${log_file}