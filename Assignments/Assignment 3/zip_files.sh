#!/bin/bash

# Source & Destination Folders
nusnetid=E0544466
studentid=A0218430N
src=src

# Create Temporary Folders
mkdir ./temp
mkdir ./temp/testcases

# Copy Report
cp ./${studentid}_report.pdf ./temp/${studentid}_report.pdf

# Copy Code
cp ./${src}/main.c ./temp/main.c
cp ./${src}/utils.h ./temp/utils.h
cp ./${src}/utils.c ./temp/utils.c

# Copy Makefile & Test Cases
cp ./${src}/Makefile ./temp/Makefile
cp -r ./${src}/sample_input_files/ ./temp/testcases/
cp -r ./${src}/sample_output_files/ ./temp/testcases/

# ZIP Folders
cd ./temp
zip -r ./${studentid}.zip ./*
cd ../
mv ./temp/${studentid}.zip ./${studentid}.zip

# Delete Temporary Folders
rm -r temp

# Run Checker
./${src}/check_zip.sh ${studentid}.zip